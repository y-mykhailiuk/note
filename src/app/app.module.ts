import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NotebookComponent} from './notebook/notebook.component';
import {LocalStorageService} from './services/local-storage.service';
import {LoadDataService} from './services/load-data.service';
import {HttpClientModule} from '@angular/common/http';
import {BsDatepickerModule, CollapseModule} from 'ngx-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FilterPipe } from './pipe/filter.pipe';

@NgModule({
    declarations: [
        AppComponent,
        NotebookComponent,
        FilterPipe
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        BsDatepickerModule.forRoot(),
        CollapseModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        LocalStorageService,
        LoadDataService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
