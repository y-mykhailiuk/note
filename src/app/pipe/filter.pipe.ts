import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {

    transform(notes: any, searchKeywords: string): any {
        if (!searchKeywords) {
            return notes;
        }
        return notes.filter(note => note.keywords.toString().toLowerCase()
            .indexOf(searchKeywords.toLowerCase()) !== -1);
    }
}

