import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class LoadDataService {

  constructor(private httpClient: HttpClient) { }
    public loadData() {
        return this.httpClient
            .get(  '../assets/notes.json' );
    }
}
