import {Component, OnInit} from '@angular/core';
import {LocalStorageService} from '../services/local-storage.service';
import {LoadDataService} from '../services/load-data.service';
import {setTheme} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
    selector: 'app-notebook',
    templateUrl: './notebook.component.html',
    styleUrls: ['./notebook.component.scss'],
})


export class NotebookComponent implements OnInit {
    bsValue = new Date();
    bsRangeValue: Date[];
    maxDate = new Date();
    public newNote: FormGroup;
    public editNote: FormGroup;
    notes;
    index = 1;
    searchKeywords: string;
    isCollapsed = true;


    constructor(private localStorageService: LocalStorageService,
                private fb: FormBuilder,
                private loadDataService: LoadDataService) {
        this.newNote = fb.group({
            'caption': [null, Validators.compose([Validators.required, Validators.maxLength(255)])],
            'text': [null, Validators.compose([Validators.required, Validators.maxLength(255)])],
            'keywords': [null, Validators.compose([Validators.required, Validators.maxLength(255)])],
            'date': [null, Validators.compose([Validators.maxLength(255)])]
        });
        this.editNote = fb.group({
            'caption': [null, Validators.compose([Validators.required, Validators.maxLength(255)])],
            'text': [null, Validators.compose([Validators.required, Validators.maxLength(255)])],
            'keywords': [null, Validators.compose([Validators.required, Validators.maxLength(255)])],
            'date': [null]
        });
        setTheme('bs3');
        this.maxDate.setDate(this.maxDate.getDate() + 7);
        this.bsRangeValue = [this.bsValue, this.maxDate];
    }

    ngOnInit() {
        this.loadDataService.loadData().subscribe(
            success => {
                this.notes = success;

            },
            error2 => {
                console.log(error2);
            }
        );
    }

    createNote() {
        let newNoteC = {
            caption: this.newNote.controls['caption'].value,
            text: this.newNote.controls['text'].value,
            keywords: this.newNote.controls['keywords'].value,
            date: (new Date().toLocaleString())
        };
        this.notes[Object.keys(this.notes).length] = newNoteC;
        this.isCollapsed = false;
        let a = document.getElementById('stored');
        a.className = 'show';
        setTimeout(() => {
            a.className = a.className.replace('show', '');
        }, 3000);
        return this.notes;
    }

    giveIndex(index) {
        this.index = index;
        this.editNote.controls['caption'].setValue(this.notes[this.index].caption);
        this.editNote.controls['text'].setValue(this.notes[this.index].text);
        this.editNote.controls['keywords'].setValue(this.notes[this.index].keywords);
    }

    editN() {
        let newNoteC = {
            caption: this.editNote.controls['caption'].value,
            text: this.editNote.controls['text'].value,
            keywords: this.editNote.controls['keywords'].value,
            date: (new Date().toLocaleString())
        };
        this.notes[this.index] = newNoteC;
        let c = document.getElementById('updated');
        c.className = 'show';
        setTimeout(() => {
            c.className = c.className.replace('show', '');
        }, 3000);
    }

    deleteNote() {
        this.notes.splice(this.index, 1);
        let b = document.getElementById('removed');
        b.className = 'show';
        setTimeout(() => {
            b.className = b.className.replace('show', '');
        }, 3000);
    }

}
